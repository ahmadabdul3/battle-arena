//
//  BaseCharacter.swift
//  battle arena
//
//  Created by Abdul on 8/27/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseCharacter : SKNode {
    
    var healthLabel = SKLabelNode(text: "100/100")
    var damageLabel = ""
    var isMoving = false
    var baseMovementSpeed:CGFloat = 1
    var weapon = ""
    var maxHp = "100"
    var hitPoints: CGFloat = 100
    var target = ""
    var isAbilitySelected = false
    var canMove = true
    
    var selectedAbilityContainer = AbilityContainer()
    //var selectedAbility = BaseAbility()
    var negativeEffect = BaseEffect()
    var positiveEffect = BaseEffect()
    var extendedDamageActionKey = "extendedDamageActionKey"
    var networkName = PlayerNameEnum.remotePlayer
    var lastEffectNumber = 0
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        super.init()
        
        self.name = "mainChar"
        addChild(getCharacterBody())
        setUpHpLabel()
        SpritePhysicsSetUpHelper().basicSetUpPhysics(self, pBody: SKPhysicsBody(rectangleOfSize: convertNodeToSprite(self.childNodeWithName("charBody")!).size), dynamic: true, categoryBitMask: PhysicsCategory.Character, contactTestBitMask: PhysicsCategory.All, collisionBitMask: PhysicsCategory.Barrier, preciseCollision: true)
    }
    
    func setUpHpLabel() {
        healthLabel.fontName = "Arial-BoldMT"
        healthLabel.fontSize = 15
        healthLabel.fontColor = UIColor.redColor()
        healthLabel.position = CGPointMake(self.position.x, self.position.y + 55)
        addChild(healthLabel)
    }
    func getCharacterBody() -> SKSpriteNode {
        let texture = SKTexture(imageNamed: "batman-sprite.png")
        let body = SKSpriteNode(texture: texture, color: nil, size: CGSize(width: 80, height: 100))
        body.name = "charBody"
        return body
    }

    func applyMovementSpeed(moveAmount: CGFloat) -> CGFloat {
        if negativeEffect.getSpeed() < 1 {
            return moveAmount * negativeEffect.getSpeed()
        }
        return moveAmount * positiveEffect.getSpeed()
    }
    func movePosition(point : CGPoint) {
            position.x += applyMovementSpeed(point.x)
            position.y += applyMovementSpeed(point.y)
    }
    func updatePosition(point: CGPoint) {
        position = point
    }
    func slideToPosition(newPosition: CGPoint) {
        var slideToNewPosition = SKAction.moveTo(newPosition, duration: 0.1)
        self.runAction(slideToNewPosition)
    }
    func convertNodeToSprite(node : SKNode) -> SKSpriteNode {
        return node as! SKSpriteNode
    }
    
    func applyPositiveAbilityEffect(effect: BaseEffect) {
        positiveEffect = effect
        applyInitialDamage(positiveEffect)
        applyExtendedDamage(positiveEffect, key: "positive")
        resetEffectWithDelay("positive", delay: positiveEffect.duration)
        
    }
    func applyAbilityEffect(effect:BaseEffect) {
        lastEffectNumber = effect.effectNumber
        negativeEffect = effect
        applyInitialDamage(negativeEffect)
        applyExtendedDamage(negativeEffect, key: extendedDamageActionKey)
        resetEffectWithDelay("negative", delay: negativeEffect.duration)
    }
    func applyInitialDamage(effect:BaseEffect) {
        deductHp(effect.damage.getInitPhys() + effect.damage.getInitSpec())
    }
    func applyExtendedDamage(effect: BaseEffect, key: String) {
        
        if effect.damage.getXtndSpec() != 0 || effect.damage.getXtndPhys() != 0 {
            if key == extendedDamageActionKey {
                self.removeActionForKey(key)
            }
            self.runAction(
                SKAction.repeatAction (
                SKAction.sequence([
                    SKAction.waitForDuration(effect.frequency),
                    SKAction.runBlock({
                        self.deductHp(effect.damage.getXtndPhys() + effect.damage.getXtndSpec())
                        return ()
                    })
                    
                    ]),
                    count : effect.getCount()
                ), withKey: key
            )
        }
    }
    
    func deductHp(amount: CGFloat) {
        println(amount)
        hitPoints -= amount
        updateHpLabel()
    }
    func resetEffectWithDelay(effectType: String, delay: NSTimeInterval) {
        runAction(
            SKAction.sequence([
                SKAction.waitForDuration(delay),
                SKAction.runBlock({
                    self.updateEffect(effectType, newEffect: BaseEffect())
                    return ()
                })
            ])
        )
    }
    func update() {
        
    }

    func updateHpLabel() {
        healthLabel.text = String(stringInterpolationSegment: hitPoints) + "/" + maxHp
    }
    
    //getters/setters
    func updateEffect(effectType: String, newEffect: BaseEffect) {
        if effectType == "positive" {
            positiveEffect = newEffect
        } else {
            negativeEffect = newEffect
        }
    }
    func updateNegEffect(effect: BaseEffect) {
        negativeEffect = effect
        
    }
    func hasAbilitySelected() -> Bool {
        return isAbilitySelected
    }
    func assignIsAbilitySelected(selected: Bool) {
        isAbilitySelected = selected
    }
    func applyCooldownOnAbility() {
        selectedAbilityContainer.resetCooldown()
    }
    func getSelectedAbility() -> BaseAbility {
        var ability = selectedAbilityContainer.getAbility()
        return ability 
    }
    func setSelectedAbilityContainerNull() {
        applyCooldownOnAbility()
        selectedAbilityContainer = AbilityContainer()
        isAbilitySelected = false
    }
    func setSelectedAbilityContainer(abilityContainer: AbilityContainer) {
        selectedAbilityContainer = abilityContainer
    }

    func getMovementSpeed() -> CGFloat {
        if negativeEffect.getSpeed() < 1 {
            return negativeEffect.getSpeed()
        }
        return positiveEffect.getSpeed()
    }
    func setNetworkName(name: PlayerNameEnum) {
        networkName = name
    }
    func getNegativeEffect() -> BaseEffect {
        return negativeEffect
    }
    
}
