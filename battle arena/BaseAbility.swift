//
//  BaseAbility.swift
//  battle arena
//
//  Created by Abdul on 8/28/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseAbility:SKNode {
    var abilityActionPerformer:IAbilityActionPerformer = DefaultAbilityActionPerformer()
    var casterName = ""
    var texture = SKNode()
    var projectileHead = SKNode()
    var effect = BaseEffect()
    var abilityName = AbilityNamesEnum.BaseAbility
    var abilityNumber:Int = 0
    var addable = false
    //var container = AbilityContainer()
    
    override init() {
        super.init()
        //container.setAbility(self)
    }
    convenience init(number: Int) {
        self.init()
        abilityNumber = number
        effect.effectNumber = number
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func performAction() { }
    func performAction(player: BaseCharacter, destination: CGPoint) { }
    func getEffect() -> BaseEffect {
        return effect
    }
    func getDestination() -> CGPoint { return abilityActionPerformer.getDestination() }
    func getDuration() -> CGFloat { return abilityActionPerformer.getDuration() }
    func assignAbilityNumber(number:Int) {
        abilityNumber = number
        effect.effectNumber = number
    }
    func getAbilityNumber() -> Int {
        return abilityNumber
    }
    func setUpAbility(casterNameArg: String, positionArg: CGPoint, abilityNumberArg: Int) {
        casterName = casterNameArg
        position = positionArg
        assignAbilityNumber(abilityNumberArg)
    }
    
    
    
    
}