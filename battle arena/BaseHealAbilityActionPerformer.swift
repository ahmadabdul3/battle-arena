//
//  BaseHealAbilityActionPerformer.swift
//  battle arena
//
//  Created by Abdul on 10/14/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseHealAbilityActionPerformer : IAbilityActionPerformer {
    
    var ability = SKNode()
    func performAbilityAction(aability: BaseAbility) { }
    func performAbilityAction(aability: BaseAbility, location: CGPoint, destination: CGPoint) { }
   
    func getDestination() -> CGPoint {
        return CGPoint()
    }
    func getDuration() -> CGFloat {
        return CGFloat()
    }
    
    func performAbilityAction(aability: BaseAbility, player: BaseCharacter, destination: CGPoint) {
        var effect = aability.effect
        player.applyPositiveAbilityEffect(effect)
        player.runAction(
            SKAction.repeatAction (
                SKAction.sequence([
                    SKAction.runBlock({
                        self.addChild(player, child: effect.image)
                        return ()
                    }),
                    SKAction.waitForDuration(effect.frequency),
                    SKAction.runBlock({
                        self.removeFromParent(effect.image)
                        return ()
                    }),
                    SKAction.waitForDuration(effect.frequency)
                    
                    ]),
                count : effect.getCount() / 2
            ), withKey: ""
        )
        
    }
    func addChild(parent: SKNode, child:SKNode) {
        parent.addChild(child)
    }
    func removeFromParent(child:SKNode) {
        child.removeFromParent()
    }
    
}