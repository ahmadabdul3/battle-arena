//
//  GameWorld.swift
//  battle arena
//
//  Created by Abdul on 8/27/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit
import Darwin

enum PlayerNameEnum {
    case localPlayer
    case remotePlayer
}
class GameWorld: SKSpriteNode, SKPhysicsContactDelegate {
    
    var physicsWorld = SKPhysicsWorld()
    var localPlayer = BaseCharacter()
    var remotePlayer = BaseCharacter()
    let baseCharArray = NSMutableArray()
    var networkingEngine = MultiplayerNetworking()
    var abilityNumber:Int = 1
    var remoteAbilityNumber:Int = 1
    var label = SKLabelNode()
    
    func asgnLocalPlayer(player: BaseCharacter) {
        localPlayer = player
        localPlayer.networkName = PlayerNameEnum.localPlayer
    }
    func asgnRemotePlayer(player: BaseCharacter) {
        remotePlayer = player
        remotePlayer.networkName = PlayerNameEnum.remotePlayer
    }
    func getPlayerByEnumName(name: PlayerNameEnum) -> BaseCharacter {
        if name == PlayerNameEnum.localPlayer { return localPlayer }
        else { return remotePlayer }
    }
    func getPlayerByName(name: String) -> BaseCharacter {
        if localPlayer.name == name {
            return localPlayer
        }
        return remotePlayer
    }
    init() {
        super.init(texture: nil, color: nil, size: CGSize(width: 1000, height: 1000))
        self.userInteractionEnabled = true
        
    }
    init(rectOfSize: CGSize) {
        super.init(texture: nil, color: UIColor.grayColor(), size: rectOfSize)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func incrementAbilityNumber() {
        abilityNumber += 1
        if abilityNumber == 20 {
            abilityNumber = 1
        }
    }
    func setSelfScale(x: CGFloat, y: CGFloat) {
        self.xScale = x
        self.yScale = y
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
    }
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        
    }
    override func touchesCancelled(touches: Set<NSObject>!, withEvent event: UIEvent!) {
        
    }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            if localPlayer.hasAbilitySelected() {
                
                var selectedAbility = localPlayer.getSelectedAbility()
                
                if selectedAbility.abilityName != AbilityNamesEnum.BaseAbility {
                    selectedAbility.setUpAbility(localPlayer.name!, positionArg: localPlayer.position, abilityNumberArg: abilityNumber)
                    if(selectedAbility.addable) {
                        addChild(selectedAbility)
                    }
                    localPlayer.setSelectedAbilityContainerNull()
                    selectedAbility.performAction(localPlayer, destination: location)
                    var destination = selectedAbility.getDestination()
                    var realDuration = selectedAbility.getDuration()
                    networkingEngine.sendAddAbility(selectedAbility.abilityName, destX: destination.x, destY: destination.y, duration: realDuration, abilityNumber: abilityNumber)
                    incrementAbilityNumber()
                }
            }
        }
    }
    func getFrostBolt() -> FrostBolt {
        var bolt = FrostBolt()
        bolt.casterName = localPlayer.name!
        return bolt
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        var firstBody: SKPhysicsBody?
        var secondBody: SKPhysicsBody?
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody!.node != nil && secondBody!.node != nil) {
            
            if ((firstBody!.categoryBitMask == PhysicsCategory.Character) &&
                (secondBody!.categoryBitMask == PhysicsCategory.Projectile)) {
                    handleCollision(firstBody?.node! as! BaseCharacter, ability: secondBody?.node! as! BaseAbility)
            } else if ((firstBody!.categoryBitMask == PhysicsCategory.Projectile) &&
                (secondBody!.categoryBitMask == PhysicsCategory.Barrier)) {
                    firstBody?.node?.removeFromParent()
            }
        }
        
    }
    func handleCollision(character: BaseCharacter, ability: BaseAbility) {
        
        if ability.casterName != character.name && ability.effect.effectNumber != getPlayerByName(character.name!).lastEffectNumber {
            
            let effect = ability.getEffect()
            let char = getPlayerByName(character.name!)
            char.applyAbilityEffect(effect)
            networkingEngine.sendCollisionDetection(getOppositePlayerName(char.networkName), effectName: effect.effectName, collisionPositionX: char.position.x, collisionPositionY: char.position.y, abilityNumber: effect.effectNumber)
            ability.removeFromParent()
            
        } else if ability.casterName != character.name {
            ability.removeFromParent()
        }
    }
    func addremovecharabilitynum(char: PlayerNameEnum) {
        label = SKLabelNode()
        label.text = String(getPlayerByEnumName(char).getNegativeEffect().effectNumber)
        label.fontColor = UIColor.redColor()
        addChildNode(label, location: CGPointMake(0, 0), zPosition: 50)
        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("removeLabel"), userInfo: nil, repeats: false)
    }
    @objc func removeLabel() {
        label.removeFromParent()
    }
    func getOppositePlayerName(name: PlayerNameEnum) -> PlayerNameEnum {
        if name == PlayerNameEnum.localPlayer {
            return PlayerNameEnum.remotePlayer
        }
        return PlayerNameEnum.localPlayer
    }
    
    func shootProjectileInDirectionWithDuration(abilityName: AbilityNamesEnum, direction:CGPoint, duration: CGFloat, abilityNumber: Int) {
        remoteAbilityNumber = abilityNumber
        var abilityMapper = BaseAbilityMapper()
        var projectile = abilityMapper.getAbilityByName(abilityName)
        projectile.assignAbilityNumber(abilityNumber)
        projectile.casterName = remotePlayer.name!
        projectile.position = remotePlayer.position
        addChild(projectile)
        projectile.runAction(
            SKAction.sequence([
                SKAction.moveTo(direction, duration: NSTimeInterval(duration)),
                SKAction.runBlock({self.removeNode(projectile); return ()})
                ])
        )
    }
    func syncCollisionDetection(playerName: PlayerNameEnum, effectName: EffectNameEnum, collisionPositionX: CGFloat, collisionPositionY: CGFloat, abilityNumber:Int) {
        let effectMapper = BaseEffectMapper()
        let effect = effectMapper.getEffectByName(effectName)
        effect.effectNumber = abilityNumber
        var playerEffectNumber = getPlayerByEnumName(playerName).lastEffectNumber
        if  playerEffectNumber != abilityNumber {
            getPlayerByEnumName(playerName).applyAbilityEffect(effect)
            //getPlayerByEnumName(playerName).position = CGPointMake(collisionPositionX, collisionPositionY)
        }
    }
    func removeNode(node:SKNode) {
        node.removeFromParent()
    }
    func addChildNode(node: SKNode,location: CGPoint, zPosition: CGFloat) {
        node.zPosition = zPosition
        node.position = location
        self.addChild(node)
    }
    
    func update() {
        updateBaseChar(localPlayer)
        
    }
    func updateBaseChar(char: BaseCharacter) {
        char.update()
    }
    
    
}
