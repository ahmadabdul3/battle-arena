//
//  BaseHeal.swift
//  battle arena
//
//  Created by Abdul on 10/14/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseHeal : BaseAbility {
    //var abilityNames = AbilityNames()
    override init() {
        super.init()
        projectileHead = SKNode()
        /*SpritePhysicsSetUpHelper().basicSetUpPhysics(self, pBody: SKPhysicsBody(circleOfRadius: convertToSprite(self.projectileHead).size.width / 2), dynamic: true, categoryBitMask: PhysicsCategory.Projectile, contactTestBitMask: PhysicsCategory.All, collisionBitMask: PhysicsCategory.Barrier, preciseCollision: true)*/
        texture = SKEmitterNode(fileNamed: "basicHeal.sks")
        addChild(texture)
        effect = BaseHealEffect()
        abilityName = AbilityNamesEnum.BaseHeal
        abilityActionPerformer = BaseHealAbilityActionPerformer()
        addable = false
    }
    convenience init(casterName: String) {
        self.init()
        self.casterName = casterName
    }
    func convertToSprite(node : SKNode) -> SKSpriteNode {
        return node as! SKSpriteNode
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func performAction() {
    }
    override func performAction(player: BaseCharacter, destination: CGPoint) {
        abilityActionPerformer.performAbilityAction(self, player: player, destination: destination)
    }
}