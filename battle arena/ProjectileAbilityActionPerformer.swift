//
//  ProjectileAbilityPerformer.swift
//  battle arena
//
//  Created by Abdul on 10/1/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class ProjectileAbilityActionPerformer : IAbilityActionPerformer {
    
    var realDestination = CGPoint()
    var realDuration = CGFloat()
    
    init() {
        ability = BaseAbility()
    }
    convenience init(theAbility:BaseAbility) {
        self.init()
        ability = theAbility
    }
    
    var ability:SKNode
    func performAbilityAction(aability: BaseAbility) {
        
    }
    func performAbilityAction(aability:BaseAbility, location:CGPoint, destination: CGPoint) {
        var distance = getDistance(location, secondLocation: destination)
        var duration = getDuration(distance, division: 200)
        var maxdestination = maximizeProjectileDestination(location, projectileDestination: destination)
        realDestination = maxdestination
        var realDistance = getDistance(maxdestination)
        var realDuration = getDuration(realDistance, division: 500)
        self.realDuration = realDuration
        
        setEmissionAngle(aability, location: location, destination: destination)
        
        aability.runAction(
            SKAction.sequence([
                SKAction.moveTo(maxdestination, duration: NSTimeInterval(realDuration)),
                SKAction.runBlock({self.ability.removeFromParent(); return ()})
                ])
        )
    }
    func setEmissionAngle(aability:BaseAbility, location: CGPoint, destination: CGPoint) {
        let deltaY = destination.y - location.y
        let deltaX = destination.x - location.x
        let angle = atan2(deltaY, deltaX)
        if aability.texture.isKindOfClass(SKEmitterNode) {
            let abilityEmitter = aability.texture as! SKEmitterNode
            aability.texture = abilityEmitter
            abilityEmitter.emissionAngle = angle + CGFloat(M_PI)
        }
    }
    func getDistance(firstLocatoin:CGPoint, secondLocation:CGPoint) -> CGFloat {
        var xminus = (secondLocation.x - firstLocatoin.x)
        var yminus = (secondLocation.y - firstLocatoin.y)
        var xpart = xminus * xminus
        var ypart = yminus * yminus
        return sqrt(xpart + ypart)
    }
    func getDistance(destination:CGPoint) -> CGFloat {
        var xpart2 = destination.x * destination.x
        var ypart2 = destination.y * destination.y
        return sqrt(xpart2 + ypart2)
    }
    func getDuration(distance: CGFloat, division:CGFloat) -> CGFloat {
        return distance / division
    }
    func maximizeProjectileDestination(playerPosition:CGPoint, projectileDestination: CGPoint) -> CGPoint {
        var xOffset = projectileDestination.x - playerPosition.x
        var yOffset = projectileDestination.y - playerPosition.y
        var xdest = xOffset * 1000
        var ydest = yOffset * 1000
        
        return CGPointMake(xdest, ydest)
    }
    func getDestination() -> CGPoint {
        return realDestination
    }
    func getDuration() -> CGFloat {
        return realDuration
    }
    func performAbilityAction(aability: BaseAbility, player: BaseCharacter, destination: CGPoint) {}
}