//
//  IAbilityPerformer.swift
//  battle arena
//
//  Created by Abdul on 10/1/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

protocol IAbilityActionPerformer {
    var ability:SKNode {get set}
    func performAbilityAction(aability: BaseAbility)
    func performAbilityAction(aability: BaseAbility, location: CGPoint, destination: CGPoint)
    func performAbilityAction(aability: BaseAbility, player: BaseCharacter, destination: CGPoint)
    func getDestination() -> CGPoint
    func getDuration() -> CGFloat
}
