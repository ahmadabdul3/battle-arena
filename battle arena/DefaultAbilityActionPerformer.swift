//
//  DefaultAbilityPerformer.swift
//  battle arena
//
//  Created by Abdul on 10/1/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class DefaultAbilityActionPerformer : IAbilityActionPerformer {
    
    var ability = SKNode()
    func performAbilityAction(aability: BaseAbility) { }
    func performAbilityAction(aability: BaseAbility, location: CGPoint, destination: CGPoint) { }
    func performAbilityAction(aability: BaseAbility, player: BaseCharacter, destination: CGPoint) { }
    func getDestination() -> CGPoint {
        return CGPoint()
    }
    func getDuration() -> CGFloat {
        return CGFloat()
    }
}
