//
//  AbilityButton.swift
//  battle arena
//
//  Created by Abdul on 10/2/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class AbilityButton:SKSpriteNode {
    //var background = SKSpriteNode(texture: nil, color: UIColor.orangeColor(), size: CGSize(width: 100, height: 100))
    var abilityContainer = AbilityContainer()
    var player = BaseCharacter()
    
    init() {
        super.init(texture: nil, color: UIColor.blueColor(), size: CGSizeMake(100, 100))
        zPosition = 30
    }
    convenience init (abilityName: AbilityNamesEnum, color: UIColor) {
        self.init()
        userInteractionEnabled = true
        zPosition = 30
        self.color = color
        self.abilityContainer.setAbilityName(abilityName)
        
    }
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
    }
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        //for touch in (touches as! Set<UITouch>) {
            //let location = touch.locationInNode(self)
            player.setSelectedAbilityContainer(abilityContainer)
            player.assignIsAbilitySelected(true)
        
        //}
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setCharacter(player: BaseCharacter) {
        self.player = player
    }
    func setAbility(abilityName: AbilityNamesEnum) {
        abilityContainer.setAbilityName(abilityName)
    }
}