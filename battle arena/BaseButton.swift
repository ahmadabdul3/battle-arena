//
//  BaseButton.swift
//  battle arena
//
//  Created by Abdul on 10/3/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseButton: SKSpriteNode {
    init() {
        super.init(texture: nil, color: nil, size: CGSizeMake(5, 5))
        userInteractionEnabled = true
        zPosition = 30
    }
    override init(texture: SKTexture, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        userInteractionEnabled = true
        zPosition = 30
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
