//
//  BaseAbilityMapper.swift
//  battle arena
//
//  Created by Abdul on 10/3/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

enum AbilityNamesEnum {
    case FrostBolt
    case FireBall
    case BaseAbility
    case BaseHeal
}

class BaseAbilityMapper {
    let abilityNames = AbilityNames()
    
    func getAbilityByName(name: AbilityNamesEnum) -> BaseAbility {
        switch name {
            case AbilityNamesEnum.FireBall: return FireBall()
            case AbilityNamesEnum.FrostBolt: return FrostBolt()
            case AbilityNamesEnum.BaseHeal: return BaseHeal()
            default: return BaseAbility()
        }
    }
}