//
//  AbilityWrapper.swift
//  battle arena
//
//  Created by Abdul on 10/2/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class AbilityWrapper {
    var abilityName = AbilityNamesEnum.BaseAbility
    var abilityMapper = BaseAbilityMapper()
    init() {
        
    }
    init(abilityName:AbilityNamesEnum) {
        self.abilityName = abilityName
    }
    func getAbility() -> BaseAbility {
        return abilityMapper.getAbilityByName(abilityName)
    }
    func setAbilityName(abilityName: AbilityNamesEnum) {
        self.abilityName = abilityName
    }
}