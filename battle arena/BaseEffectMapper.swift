//
//  BaseEffectMapper.swift
//  battle arena
//
//  Created by Abdul on 10/4/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation


enum EffectNameEnum {
    case BaseEffect
    case FreezeEffect
    case FireballEffect
    case BaseHealEffect
    
}
class BaseEffectMapper {
    
    func getEffectByName(name: EffectNameEnum) -> BaseEffect {
        switch(name) {
            case EffectNameEnum.FreezeEffect: return FreezeEffect()
            case EffectNameEnum.FireballEffect: return FireballEffect()
            case EffectNameEnum.BaseHealEffect: return BaseHealEffect()
            default: return BaseEffect()
        }
    }
}