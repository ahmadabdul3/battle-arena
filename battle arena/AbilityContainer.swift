//
//  AbilityContainer.swift
//  battle arena
//
//  Created by Abdul on 10/2/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class AbilityContainer {
    var abilityWrapper = AbilityWrapper()
    var inCooldown = false
    init() {
        
    }
    init(abilityName: AbilityNamesEnum) {
        self.abilityWrapper.abilityName = abilityName
    }
    func setAbilityName(abilityName:AbilityNamesEnum) {
        self.abilityWrapper.abilityName = abilityName
    }
    func getAbility() -> BaseAbility {
        if !inCooldown {
            //println("not in cooldown")
            return abilityWrapper.getAbility()
        }
        //println("in cooldown")
        return BaseAbility()
    }
    func isInCooldown() -> Bool {
        return inCooldown
    }
    func resetCooldown() {
        inCooldown = true
        NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: Selector("cooldownComplete"), userInfo: nil, repeats: false)
    }
    @objc func cooldownComplete() {
        inCooldown = false
    }
}