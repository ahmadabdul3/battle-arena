//
//  BaseHealEffect.swift
//  battle arena
//
//  Created by Abdul on 10/14/15.
//  Copyright (c) 2015 3bdugames. All rights reserved.
//

import Foundation
import SpriteKit

class BaseHealEffect : BaseEffect {
    
    override init() {
        super.init()
        movement = Movement(speed: 1)
        damage = Damage(initPhys: 0, xtndPhys: 0, initSpec: -5, xtndSpec: -2)
        duration = 4
        frequency = 1
        effectName = EffectNameEnum.BaseHealEffect
        image = SKEmitterNode(fileNamed: "basicHeal.sks")
    }
    
}